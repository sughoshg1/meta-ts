DEPENDS += "python3-pycryptodomex-native"

FILESEXTRAPATHS:prepend := "${THISDIR}/files/optee-os/zynqmp:"

COMPATIBLE_MACHINE:zynqmp-kria-starter = "zynqmp-kria-starter"

OPTEEMACHINE = "zynqmp-ultra96"
EXTRA_OEMAKE += " ARCH=arm"

# Uncoment to enable verbose logs
# EXTRA_OEMAKE += " CFG_TEE_CORE_LOG_LEVEL=4 CFG_EARLY_CONSOLE=1"

# default disable latency benchmarks (over all OP-TEE layers)
EXTRA_OEMAKE +=  " CFG_TEE_BENCHMARK=n"

EXTRA_OEMAKE += " CFG_ARM64_core=y"

EXTRA_OEMAKE += " HOST_PREFIX=${HOST_PREFIX}"
EXTRA_OEMAKE += " CROSS_COMPILE64=${HOST_PREFIX}"
EXTRA_OEMAKE += " CFG_DDR_SIZE=0x100000000 DRAM1_BASE=0x800000000"

FILES:${PN} = "/lib/firmware"
SYSROOT_DIRS += "/lib/firmware"

FILES:${PN}-dbg = "/lib/firmware/*.elf"
# Skip QA check for relocations in .text of elf binaries
INSANE_SKIP:${PN}-dbg = "textrel"
