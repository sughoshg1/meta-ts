# Machine specific configurations

MACHINE_OPTEE_OS_REQUIRE ?= ""
MACHINE_OPTEE_OS_REQUIRE:synquacer = "optee-os-tadevkit-synquacer.inc"
MACHINE_OPTEE_OS_REQUIRE:stm32mp157c-dk2 = "optee-os-tadevkit-stm32mp157c-dk2.inc"
MACHINE_OPTEE_OS_REQUIRE:stm32mp157c-ev1 = "optee-os-tadevkit-stm32mp157c-ev1.inc"
MACHINE_OPTEE_OS_REQUIRE:rockpi4b = "optee-os-tadevkit-rockpi4b.inc"

require ${MACHINE_OPTEE_OS_REQUIRE}

# default now in meta-arm, not ok for meta-ts firmware
EXTRA_OEMAKE:remove = "CFG_MAP_EXT_DT_SECURE=y"
