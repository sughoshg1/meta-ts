# Generate zynqmp-zcu102 kit style loader binaries
inherit deploy

SRC_URI += "file://zynqmp_fsbl.elf"
SRC_URI += "file://pmufw.elf"
SRC_URI += "file://0001-u-boot-zynqmp-Disable-the-hub-node-from-the-usb-nide.patch"
SRC_URI += "file://extra_opts.cfg"
SRC_URI += "file://secure_boot.cfg"

COMPATIBLE_MACHINE = "zynqmp-zcu102"

DEPENDS += "trusted-firmware-a"
DEPENDS += "xxd-native"
DEPENDS += "u-boot-mkimage-native bootgen-native"

UBOOT_BOARDDIR = "${S}/board/xilinx/zynqmp"
UBOOT_ENV_NAME = "zynqmp.env"

do_compile:prepend() {
    cp ${S}/../pmufw.elf ${B}/xilinx_zynqmp_virt_defconfig/
    cp ${S}/../zynqmp_fsbl.elf ${B}/xilinx_zynqmp_virt_defconfig/
    cp ${RECIPE_SYSROOT}/firmware/bl31.elf ${B}/xilinx_zynqmp_virt_defconfig/
}

do_deploy:append() {
    cd ${B}/xilinx_zynqmp_virt_defconfig/

    cat <<EOF > bootgen.bif
    the_ROM_image:
    {
    [ bootloader,
      destination_cpu=a53-0
    ] zynqmp_fsbl.elf
    [ destination_cpu = pmu
    ] pmufw.elf
    [ destination_cpu=a53-0,
      exception_level=el-3,
      trustzone=secure
    ] bl31.elf
    [ destination_cpu=a53-0,
      exception_level=el-2
    ] u-boot.elf
    [type=raw, load=0x100000] arch/arm/dts/zynqmp-zcu102-rev1.0.dtb
    }
EOF

    bootgen -image bootgen.bif -arch zynqmp -r -w -o xilinx_boot.bin

    cd -
    cp ${B}/xilinx_zynqmp_virt_defconfig/xilinx_boot.bin ${DEPLOYDIR}/boot.bin
}

ATF_DEPENDS = " trusted-firmware-a:do_deploy"
do_compile[depends] .= "${ATF_DEPENDS}"
