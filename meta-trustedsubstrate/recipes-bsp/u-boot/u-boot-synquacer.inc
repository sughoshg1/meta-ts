FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"
FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}/synquacer:"

SRC_URI += "file://synquacer_developerbox_defconfig"

UBOOT_BOARDDIR = "${S}/board/socionext/developerbox"
UBOOT_ENV_NAME = "developerbox.env"
