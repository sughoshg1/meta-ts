FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:${THISDIR}/u-boot/common:"

# Always increment PR on u-boot config change or patches
PR .= "ts3"

# for tools/mkeficapsule
DEPENDS += "gnutls-native"

SRC_URI += "file://0002-fix-smbios-tables.patch"
SRC_URI += "file://0003-fix-authenticated-capsules.patch"
SRC_URI += "file://trs.env"

do_configure:prepend() {
	cp -r ${WORKDIR}/*_defconfig ${S}/configs/ || true
}

do_configure:append() {
	if [ -e "${UEFI_CAPSULE_CERT_FILE}" ]; then
		mkdir -p uefi_capsule_certs
		tar xpvfz ${UEFI_CAPSULE_CERT_FILE} -C uefi_capsule_certs
		echo CONFIG_EFI_CAPSULE_ESL_FILE=\"${B}/uefi_capsule_certs/CRT.esl\" >> ${B}/${config}/.config
	fi

	if [ "${@bb.utils.contains('MACHINE_FEATURES', 'disable-console', '1', '0', d)}" = "1" ] ; then
		echo "CONFIG_BOOTMENU_DISABLE_UBOOT_CONSOLE=y" >> ${B}/${config}/.config
		echo "CONFIG_AUTOBOOT_MENU_SHOW=y" >> ${B}/${config}/.config
	fi

	if [ "${@bb.utils.contains('MACHINE_FEATURES', 'silence-console', '1', '0', d)}" = "0" ] ; then
		sed -i '/silent=1/d' ${WORKDIR}/trs.env
	fi

	cp ${WORKDIR}/trs.env ${UBOOT_BOARDDIR}/${UBOOT_ENV_NAME}
}

MACHINE_UBOOT_REQUIRE ?= ""

MACHINE_UBOOT_REQUIRE:rockpi4b = "u-boot-rockpi4b.inc"
MACHINE_UBOOT_REQUIRE:rpi4 = "u-boot-rpi4.inc"
MACHINE_UBOOT_REQUIRE:synquacer = "u-boot-synquacer.inc"
MACHINE_UBOOT_REQUIRE:tsqemuarm-secureboot = "u-boot-qemuarm-secureboot.inc"
MACHINE_UBOOT_REQUIRE:tsqemuarm64-secureboot = "u-boot-qemuarm64-secureboot.inc"
MACHINE_UBOOT_REQUIRE:stm32mp157c-dk2 = "u-boot-stm32mp157c-dk2.inc"
MACHINE_UBOOT_REQUIRE:stm32mp157c-ev1 = "u-boot-stm32mp157c-ev1.inc"
MACHINE_UBOOT_REQUIRE:zynqmp-kria-starter = "u-boot-zynqmp-kria-starter.inc"
MACHINE_UBOOT_REQUIRE:zynqmp-zcu102 = "u-boot-zynqmp-zc102.inc"

require ${MACHINE_UBOOT_REQUIRE}

require u-boot-certs.inc
