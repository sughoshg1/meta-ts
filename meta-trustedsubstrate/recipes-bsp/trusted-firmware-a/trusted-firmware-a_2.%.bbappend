# Machine specific TFAs

FILESEXTRAPATHS:prepend := "${THISDIR}/files/:"

# Enable mbed TLS support
TFA_MBEDTLS = "1"

# try to find correct libcrypto.so.3
do_compile:prepend() {
    export LD_LIBRARY_PATH="${STAGING_LIBDIR_NATIVE}"
}

EXTRA_OEMAKE += " LOG_LEVEL=30"

MACHINE_TFA_REQUIRE ?= ""

MACHINE_TFA_REQUIRE:synquacer = "trusted-firmware-a-synquacer.inc"
MACHINE_TFA_REQUIRE:stm32mp157c-dk2 = "trusted-firmware-a-stm32mp157c-dk2.inc"
MACHINE_TFA_REQUIRE:stm32mp157c-ev1 = "trusted-firmware-a-stm32mp157c-ev1.inc"
MACHINE_TFA_REQUIRE:rockpi4b = "trusted-firmware-a-rockpi4b.inc"
MACHINE_TFA_REQUIRE:zynqmp-kria-starter = "trusted-firmware-a-zynqmp.inc"
MACHINE_TFA_REQUIRE:zynqmp-zcu102 = "trusted-firmware-a-zynqmp.inc"
MACHINE_TFA_REQUIRE:tsqemuarm64-secureboot = "trusted-firmware-a-tsqemuarm64-secureboot.inc"
MACHINE_TFA_REQUIRE:tsqemuarm-secureboot = "trusted-firmware-a-tsqemuarm-secureboot.inc"

require ${MACHINE_TFA_REQUIRE}
